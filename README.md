kandooit_calendar
=========================

An events calendar component built for React and made for modern browsers (read: IE10+) and uses flexbox over the classic tables-ception approach.

[__DEMO and Docs__](https://bitbucket.org/jn-kandooit/kandooit_calendar)

Inspired by [Full Calendar](http://intljusticemission.github.io/react-big-calendar/examples/index.html and http://fullcalendar.io/).

## Use and Setup

`npm install kandooit_calendar --save`

Include `kandooit_calendar/lib/css/react-big-calendar.css` for styles.
## Drag and Drop

import withDragAndDrop from 'kandooit_calendar/lib/addons/dragAndDrop';


BigCalendar.setLocalizer(
  BigCalendar.momentLocalizer(moment)
);

const DragAndDropCalendar = withDragAndDrop(BigCalendar);
then use this DragAndDropCalendar like below:

            <DragAndDropCalendar
              selectable
              resizable
              min={am8}
              max={pm8}
              timeslots={1}
              step={60}
              events={this.state.appointments}
              statusHeadings={this.props.MasterStatusHeadings}
              resources={this.state.resource_list}
              defaultView={this.filter.currentView}
              scrollToTime={new Date(1970, 1, 1, 6)}
              defaultDate={new Date()}
              date={this.state.schedulerDate}
              formats={formats}
              views={scheduler_view}
              onEventResize={this.resizeAppointment}
              draggableAccessor= 'isDragable'
              resizableAccessor= 'isDragable'
              onEventDrop={(event) => this.moveAppointment(event)}
              onSelectEvent={(event) => this.openDialog(event)}
              onView={(e) => this.handleStaffView(e)}
              eventPropGetter={(event, date, end, isSelected) => this.eventStyleGetter(event)}
              onSelectSlot={(slotInfo) => this.openDialog(slotInfo)}
              onNavigate={this.onNavigateMonth}
              usersAvailability={filteredUsersAvailability}
            />

## Run examples locally

* Clone this repository
* Retrieve dependencies: `npm install`
* Start: `npm run examples`
* Open [localhost:3003/examples/index.html](http://localhost:3003/examples/index.html).

### Localization and Date Formatting

`react-big-calendar` includes two options for handling the date formatting and culture localization, depending
on your preference of DateTime libraries. You can use either the [Moment.js](http://momentjs.com/) or [Globalize.js](https://github.com/jquery/globalize) localizers.

Regardless of your choice, you __must__ choose a localizer to use this library:

#### Moment.js

```js
import BigCalendar from 'react-big-calendar';
import moment from 'moment';

BigCalendar.setLocalizer(
  BigCalendar.momentLocalizer(moment)
);
```

#### Globalize.js v0.1.1

```js
import BigCalendar from 'react-big-calendar';
import globalize from 'globalize';

BigCalendar.setLocalizer(
  BigCalendar.globalizeLocalizer(globalize)
);
```
