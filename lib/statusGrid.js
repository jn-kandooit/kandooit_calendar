"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _classnames = _interopRequireDefault(require("classnames"));

var _reactDom = require("react-dom");

var _propTypes = _interopRequireDefault(require("prop-types"));

var _dates = _interopRequireDefault(require("./utils/dates"));

var _localizer = _interopRequireDefault(require("./localizer"));

var _DayColumn = _interopRequireDefault(require("./DayColumn"));

var _TimeColumn = _interopRequireDefault(require("./TimeColumn"));

var _Header = _interopRequireDefault(require("./Header"));

var _width = _interopRequireDefault(require("dom-helpers/query/width"));

var _scrollbarSize = _interopRequireDefault(require("dom-helpers/util/scrollbarSize"));

var _propTypes2 = require("./utils/propTypes");

var _helpers = require("./utils/helpers");

var _accessors = require("./utils/accessors");

var _eventLevels = require("./utils/eventLevels");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var ResourceGrid =
/*#__PURE__*/
function (_Component) {
  _inherits(ResourceGrid, _Component);

  function ResourceGrid(props) {
    var _this;

    _classCallCheck(this, ResourceGrid);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(ResourceGrid).call(this, props));

    _defineProperty(_assertThisInitialized(_this), "handleSelectAllDaySlot", function (slots) {
      var onSelectSlot = _this.props.onSelectSlot;
      (0, _helpers.notify)(onSelectSlot, {
        slots: slots,
        start: slots[0],
        end: slots[slots.length - 1]
      });
    });

    _this.state = {
      gutterWidth: undefined,
      isOverflowing: null
    };
    _this.handleSelectEvent = _this.handleSelectEvent.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(ResourceGrid, [{
    key: "componentWillMount",
    value: function componentWillMount() {
      this._gutters = [];
      this.calculateScroll();
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      this.checkOverflow();

      if (this.props.width == null) {
        this.measureGutter();
      }

      this.applyScroll();
      this.positionTimeIndicator();
      this.triggerTimeIndicatorUpdate();
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      window.clearTimeout(this._timeIndicatorTimeout);
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      if (this.props.width == null && !this.state.gutterWidth) {
        this.measureGutter();
      }

      this.applyScroll();
      this.positionTimeIndicator();
    }
  }, {
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      var _this$props = this.props,
          start = _this$props.start,
          scrollToTime = _this$props.scrollToTime; // When paginating, reset scroll

      if (!_dates.default.eq(nextProps.start, start, 'minute') || !_dates.default.eq(nextProps.scrollToTime, scrollToTime, 'minute')) {
        this.calculateScroll();
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props2 = this.props,
          events = _this$props2.events,
          start = _this$props2.start,
          end = _this$props2.end,
          width = _this$props2.width,
          startAccessor = _this$props2.startAccessor,
          endAccessor = _this$props2.endAccessor,
          allDayAccessor = _this$props2.allDayAccessor;
      width = width || this.state.gutterWidth;

      var range = _dates.default.range(start, end, 'day');

      this.slots = range.length;
      var allDayEvents = [],
          rangeEvents = [];
      events.forEach(function (event) {
        if ((0, _eventLevels.inRange)(event, start, end, _this2.props)) {
          var eStart = (0, _accessors.accessor)(event, startAccessor),
              eEnd = (0, _accessors.accessor)(event, endAccessor);

          if ((0, _accessors.accessor)(event, allDayAccessor) || !_dates.default.eq(eStart, eEnd, 'day') || _dates.default.isJustDate(eStart) && _dates.default.isJustDate(eEnd)) {
            allDayEvents.push(event);
          } else rangeEvents.push(event);
        }
      });
      allDayEvents.sort(function (a, b) {
        return (0, _eventLevels.sortEvents)(a, b, _this2.props);
      });

      var gutterRef = function gutterRef(ref) {
        return _this2._gutters[1] = ref && (0, _reactDom.findDOMNode)(ref);
      };

      return _react.default.createElement("div", {
        className: "rbc-time-view"
      }, this.renderResourceHeader(range, allDayEvents, width, this.props.statusHeadings), _react.default.createElement("div", {
        ref: "content",
        className: "rbc-time-content"
      }, _react.default.createElement("div", {
        ref: "timeIndicator",
        className: "rbc-current-time-indicator"
      }), _react.default.createElement(_TimeColumn.default, _extends({}, this.props, {
        showLabels: true,
        style: {
          width: width
        },
        ref: gutterRef,
        isGutter: true,
        className: "rbc-time-gutter"
      })), this.props.statusHeadings.map(function (resource) {
        return _this2.renderEvents(range, rangeEvents, _this2.props.now, resource.id);
      })));
    }
  }, {
    key: "renderEvents",
    value: function renderEvents(range, events, today, id) {
      var _this3 = this;

      var _this$props3 = this.props,
          min = _this$props3.min,
          max = _this$props3.max,
          endAccessor = _this$props3.endAccessor,
          startAccessor = _this$props3.startAccessor,
          components = _this$props3.components;
      return range.map(function (date, idx) {
        var _extends2;

        var daysEvents = events.filter(function (event) {
          return _dates.default.inRange(date, (0, _accessors.accessor)(event, startAccessor), (0, _accessors.accessor)(event, endAccessor), 'day') && event.statusId === id;
        });
        return _react.default.createElement(_DayColumn.default, _extends({}, _this3.props, (_extends2 = {
          key: id,
          resource: id,
          min: _dates.default.merge(date, min),
          max: _dates.default.merge(date, max),
          eventComponent: components.event,
          eventWrapperComponent: components.eventWrapper,
          dayWrapperComponent: components.dayWrapper,
          className: (0, _classnames.default)({
            'rbc-now': _dates.default.eq(date, today, 'day')
          }),
          style: (0, _eventLevels.segStyle)(1, _this3.slots)
        }, _defineProperty(_extends2, "key", idx), _defineProperty(_extends2, "date", date), _defineProperty(_extends2, "events", daysEvents), _extends2)));
      });
    }
  }, {
    key: "renderResourceHeader",
    value: function renderResourceHeader(range, events, width, statusHeadings) {
      var _this4 = this;

      var rtl = this.props.rtl;

      var _ref = this.state || {},
          isOverflowing = _ref.isOverflowing;

      var style = {};
      if (isOverflowing) style[rtl ? 'marginLeft' : 'marginRight'] = (0, _scrollbarSize.default)() + 'px';
      return _react.default.createElement("div", {
        ref: "headerCell",
        className: (0, _classnames.default)('rbc-time-header', isOverflowing && 'rbc-overflowing'),
        style: style
      }, _react.default.createElement("div", {
        className: "rbc-row"
      }, _react.default.createElement("div", {
        className: "rbc-label rbc-header-gutter",
        style: {
          width: width
        }
      }), statusHeadings.map(function (resource) {
        return _this4.renderHeaderCells(range, resource.id, resource.title);
      })));
    }
  }, {
    key: "renderHeaderCells",
    value: function renderHeaderCells(range, id, title) {
      var _this5 = this;

      var _this$props4 = this.props,
          dayFormat = _this$props4.dayFormat,
          culture = _this$props4.culture,
          components = _this$props4.components;
      var HeaderComponent = components.header || _Header.default;
      return range.map(function (date, i) {
        var label = _localizer.default.format(date, dayFormat, culture);

        var header = _react.default.createElement(HeaderComponent, {
          date: date,
          label: label,
          localizer: _localizer.default,
          format: dayFormat,
          culture: culture
        });

        return _react.default.createElement("div", {
          key: i,
          className: (0, _classnames.default)('rbc-header', _dates.default.isToday(date) && 'rbc-today'),
          style: (0, _eventLevels.segStyle)(1, _this5.slots)
        }, _react.default.createElement("span", null, title || header));
      });
    }
  }, {
    key: "handleSelectEvent",
    value: function handleSelectEvent() {
      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      (0, _helpers.notify)(this.props.onSelectEvent, args);
    }
  }, {
    key: "clearSelection",
    value: function clearSelection() {
      clearTimeout(this._selectTimer);
      this._pendingSelection = [];
    }
  }, {
    key: "measureGutter",
    value: function measureGutter() {
      var width = this.state.gutterWidth;

      var gutterCells = this._gutters.filter(function (g) {
        return !!g;
      });

      if (!width) {
        width = Math.max.apply(Math, _toConsumableArray(gutterCells.map(_width.default)));

        if (width) {
          this.setState({
            gutterWidth: width
          });
        }
      }
    }
  }, {
    key: "applyScroll",
    value: function applyScroll() {
      if (this._scrollRatio) {
        var content = this.refs.content;
        content.scrollTop = content.scrollHeight * this._scrollRatio; // Only do this once

        this._scrollRatio = null;
      }
    }
  }, {
    key: "calculateScroll",
    value: function calculateScroll() {
      var _this$props5 = this.props,
          min = _this$props5.min,
          max = _this$props5.max,
          scrollToTime = _this$props5.scrollToTime;

      var diffMillis = scrollToTime - _dates.default.startOf(scrollToTime, 'day');

      var totalMillis = _dates.default.diff(max, min);

      this._scrollRatio = diffMillis / totalMillis;
    }
  }, {
    key: "checkOverflow",
    value: function checkOverflow() {
      var _this6 = this;

      if (this._updatingOverflow) return;
      var isOverflowing = this.refs.content.scrollHeight > this.refs.content.clientHeight;

      if (this.state.isOverflowing !== isOverflowing) {
        this._updatingOverflow = true;
        this.setState({
          isOverflowing: isOverflowing
        }, function () {
          _this6._updatingOverflow = false;
        });
      }
    }
  }, {
    key: "positionTimeIndicator",
    value: function positionTimeIndicator() {
      var _this$props6 = this.props,
          rtl = _this$props6.rtl,
          min = _this$props6.min,
          max = _this$props6.max;
      var now = new Date();

      var secondsGrid = _dates.default.diff(max, min, 'seconds');

      var secondsPassed = _dates.default.diff(now, min, 'seconds');

      var timeIndicator = this.refs.timeIndicator;
      var factor = secondsPassed / secondsGrid;
      var timeGutter = this._gutters[this._gutters.length - 1];

      if (timeGutter && now >= min && now <= max) {
        var pixelHeight = timeGutter.offsetHeight;
        var offset = Math.floor(factor * pixelHeight);
        timeIndicator.style.display = 'block';
        timeIndicator.style[rtl ? 'left' : 'right'] = 0;
        timeIndicator.style[rtl ? 'right' : 'left'] = timeGutter.offsetWidth + 'px';
        timeIndicator.style.top = offset + 'px';
      } else {
        timeIndicator.style.display = 'none';
      }
    }
  }, {
    key: "triggerTimeIndicatorUpdate",
    value: function triggerTimeIndicatorUpdate() {
      var _this7 = this;

      // Update the position of the time indicator every minute
      this._timeIndicatorTimeout = window.setTimeout(function () {
        _this7.positionTimeIndicator();

        _this7.triggerTimeIndicatorUpdate();
      }, 60000);
    }
  }]);

  return ResourceGrid;
}(_react.Component);

exports.default = ResourceGrid;

_defineProperty(ResourceGrid, "propTypes", {
  events: _propTypes.default.array.isRequired,
  statusHeadings: _propTypes.default.array.isRequired,
  step: _propTypes.default.number,
  start: _propTypes.default.instanceOf(Date),
  end: _propTypes.default.instanceOf(Date),
  min: _propTypes.default.instanceOf(Date),
  max: _propTypes.default.instanceOf(Date),
  now: _propTypes.default.instanceOf(Date),
  scrollToTime: _propTypes.default.instanceOf(Date),
  eventPropGetter: _propTypes.default.func,
  dayFormat: _propTypes2.dateFormat,
  culture: _propTypes.default.string,
  rtl: _propTypes.default.bool,
  width: _propTypes.default.number,
  titleAccessor: _propTypes2.accessor.isRequired,
  allDayAccessor: _propTypes2.accessor.isRequired,
  startAccessor: _propTypes2.accessor.isRequired,
  endAccessor: _propTypes2.accessor.isRequired,
  selected: _propTypes.default.object,
  selectable: _propTypes.default.oneOf([true, false, 'ignoreEvents']),
  onNavigate: _propTypes.default.func,
  onSelectSlot: _propTypes.default.func,
  onSelectEnd: _propTypes.default.func,
  onSelectStart: _propTypes.default.func,
  onSelectEvent: _propTypes.default.func,
  onDrillDown: _propTypes.default.func,
  messages: _propTypes.default.object,
  components: _propTypes.default.object.isRequired,
  businessHours: _propTypes.default.array
});

_defineProperty(ResourceGrid, "defaultProps", {
  step: 30,
  min: _dates.default.startOf(new Date(), 'day'),
  businessHours: [],
  max: _dates.default.endOf(new Date(), 'day'),
  scrollToTime: _dates.default.startOf(new Date(), 'day'),

  /* these 2 are needed to satisfy requirements from TimeColumn required props
   * There is a strange bug in React, using ...TimeColumn.defaultProps causes weird crashes
   */
  type: 'gutter',
  now: new Date()
});